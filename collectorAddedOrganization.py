from linkedin_functions import *
import pandas as pd
import numpy as np
import itertools

organizations = pd.read_csv("organizationCounts.csv")
organizations = organizations[organizations["Count"] > 0]
organizations = organizations["Organization"].to_list()
organizations.remove("The American Speech-Language-Hearing Association (ASHA)")
organizations.remove("Community Options, Inc.")
organizations.remove("Disability Services Australia (DSA)")
organizations.remove("Handicap International - Humanity & Inclusion")

print(len(organizations))

countries = pd.read_csv("reducedCountries.csv")
countries = countries["Country"].to_list()

organization_dict = {org:[encodeFacet([org], kind='employers')] for org in organizations}

location_dict = {loc:[encodeFacet([loc], kind='locations')] for loc in countries}

columns = ['Country', 'Organization', 'Count']
new_rows = []

segments = list(itertools.product(*[location_dict, organization_dict]))
print(len(segments))

for segment in segments:
    # unpack the tuple and get encodings
    location_name, organization_name = segment
    location, organization = location_dict[location_name], organization_dict[organization_name]
    
    # generate URL for request
    requestCriteria = createRequestDataForAudienceCounts(locations = location, company_names = organization)
    
    # submit GET request
    count = getAudienceCounts(requestCriteria)
    
    # store data in a series (row)
    new_row = pd.Series(dtype=object)
    new_row['Country'] = location_name
    new_row['Organization'] = organization_name
    new_row['Count'] = count
    new_rows.append(new_row.values)

df = pd.DataFrame(new_rows, columns=columns)
df.to_csv("thirdLevel.csv", index=False)
