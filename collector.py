from linkedin_functions import *
import pandas as pd
import numpy as np
import itertools

un_countries = pd.read_csv("UN_countries.csv")
countries = un_countries["Country"].to_list()
countries.remove("Congo (DRC)")
countries.remove("Côte d'Ivoire")

gender_dict = {'Male': ['male'], 
                'Female': ['female']}

industries=['Services for the Elderly and Disabled']

location_dict = {loc:[encodeFacet([loc], kind='locations')] for loc in countries}

gender_dict = {k:[encodeFacet(genders, kind='genders')] for k, genders in gender_dict.items()}

industry_dict = {industry:[encodeFacet([industry], kind='industries')] for industry in industries}
industry_dict['All'] = [None]

segments = list(itertools.product(*[location_dict, gender_dict, industry_dict]))
print(len(segments))

columns = ['Country', 'Gender', 'Industry', 'Count']
new_rows = []

for segment in segments:
    # unpack the tuple and get encodings
    location_name, gender_name, industry_name = segment
    location, gender, industry = location_dict[location_name], gender_dict[gender_name], industry_dict[industry_name]
    
    # generate URL for request
    requestCriteria = createRequestDataForAudienceCounts(locations = location,
                                                         genders = gender,
                                                         industries= industry)
    
    # submit GET request
    count = getAudienceCounts(requestCriteria)
    
    # store data in a series (row)
    new_row = pd.Series(dtype=object)
    new_row['Country'] = location_name
    new_row['Gender'] = gender_name
    new_row['Industry'] = industry_name
    new_row['Count'] = count
    new_rows.append(new_row.values)
    
# construct dataframe
df = pd.DataFrame(new_rows, columns=columns)
df.to_csv("firstLevel.csv", index=False)
