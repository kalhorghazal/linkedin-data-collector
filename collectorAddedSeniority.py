from linkedin_functions import *
import pandas as pd
import numpy as np
import itertools

countries = pd.read_csv("reducedCountries.csv")
countries = countries["Country"].to_list()

seniorities = ["Manager", "Director"]

gender_dict = {'Male': ['male'], 
                'Female': ['female']}

industries=['Services for the Elderly and Disabled']

location_dict = {loc:[encodeFacet([loc], kind='locations')] for loc in countries}

seniority_dict = {seniority:[encodeFacet([seniority], kind='seniorities')] for seniority in seniorities}
seniority_dict['All'] = [None]

gender_dict = {k:[encodeFacet(genders, kind='genders')] for k, genders in gender_dict.items()}

industry_dict = {industry:[encodeFacet([industry], kind='industries')] for industry in industries}
industry_dict['All'] = [None]

segments = list(itertools.product(*[location_dict, gender_dict, industry_dict, seniority_dict]))
print(len(segments))

columns = ['Country', 'Gender', 'Industry', 'Seniority', 'Count']
new_rows = []

for segment in segments:
    # unpack the tuple and get encodings
    location_name, gender_name, industry_name, seniority_name = segment
    location, gender, industry, seniority = location_dict[location_name], gender_dict[gender_name], industry_dict[industry_name], seniority_dict[seniority_name]
    
    # generate URL for request
    requestCriteria = createRequestDataForAudienceCounts(locations = location,
                                                         genders = gender,
                                                         industries= industry,
                                                         seniorities= seniority)
    
    # submit GET request
    count = getAudienceCounts(requestCriteria)
    
    # store data in a series (row)
    new_row = pd.Series(dtype=object)
    new_row['Country'] = location_name
    new_row['Gender'] = gender_name
    new_row['Industry'] = industry_name
    new_row['Seniority'] = seniority_name
    new_row['Count'] = count
    new_rows.append(new_row.values)
    
# construct dataframe
df = pd.DataFrame(new_rows, columns=columns)
df.to_csv("secondLevel.csv", index=False)
